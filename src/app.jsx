import React from 'react';
import '../styles/index.scss';

import Header from './components/Header';
import AddTodo from './containers/AddTodo';
import Todos from './components/Todos';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <AddTodo />
        <Todos />
      </div>
    )
  }
}
