import React, { Component } from 'react';
import { connect } from 'react-redux';

import { addTodo } from '../actions';

class AddTodo extends Component {
  constructor () {
    super ();
    this.state = {
      todoItem: ""
    }
  }

  /**
   * Handler function for submit form
   */

  _onSubmit (event) {
    event.preventDefault();
    const { dispatch } = this.props;
    dispatch(addTodo(this.state.todoItem));
    this.setState({
      todoItem: ''
    })
  }

  /**
   * Handler function for change in input element
   */

  _handleChange (event) {
    const todoItem = event.target.value;

    this.setState({
      todoItem
    });
  }


  render () {
    return (
      <div className="container">
        <form onSubmit={this._onSubmit.bind(this)}>
          <div className="form-group">
            <input
              type="text"
              className="form-control"
              id="todo"
              value={this.state.todoItem}
              placeholder="To add a todo, add text and click on submit or press enter"
              onChange={this._handleChange.bind(this)}
              />
            </div>

          <button type="submit" className="btn btn-primary">Add</button>

        </form>
      </div>
    )
  }
}

export default connect()(AddTodo);
