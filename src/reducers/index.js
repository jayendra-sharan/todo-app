let initialState = {
  todos: [
    {
      "id": "1234",
      "text": "Go through code",
      "completed": false
    },
    {
      "id": "1235",
      "text": "Add new feature",
      "completed": true
    }
  ]
}

import {
  ADD_TODO, DELETE_TODO,
  TOGGLE_TODO
} from '../actions';

const todo = (state = {}, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        id: action.id,
        text: action.text,
        completed: false
      };

    case TOGGLE_TODO:
      if (state.id !== action.id) {
        return state;
      }
      return Object.assign({}, state,
        {
          completed: !state.completed
        }
      );

    default:
    return state;
  }
}

const todos = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return Object.assign({}, state,
        {
          todos: [...state.todos, todo(undefined, action)]
        }
      );

    case TOGGLE_TODO:
      return Object.assign({}, state, {
          todos: state.todos.map(t => todo(t, action))
      });

    case DELETE_TODO:
      return Object.assign({}, state, {
          todos: state.todos.filter(t => t.id !== action.id)
      });

    default:
      return state;
  }
}

export default todos;
