import React, { Component } from 'react';
import { connect } from 'react-redux';

import TodoItem from '../containers/TodoItem';

class Todos extends Component {
  constructor () {
    super ();
  }

  render () {
    const {todos} = this.props;
    return (
      <ul className="todo-list">
        {todos.map(todo => {
          return (
            <TodoItem
              key={todo.id}
              text={todo.text}
              completed={todo.completed}
              id={todo.id}
              />
          )

        })}
      </ul>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todos
  }
}

export default connect(mapStateToProps)(Todos);
